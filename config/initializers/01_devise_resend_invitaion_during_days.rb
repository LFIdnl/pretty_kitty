require 'devise'

module DeviseResendInvitaionDuringDays
  extend ActiveSupport::Concern

  included do
    mattr_accessor :resend_invitation_during_days
    @@resend_invitation_during_days = 0
  end
end

Devise.send(:include, DeviseResendInvitaionDuringDays)
