Rails.application.routes.draw do
  devise_for :users, :controllers => { :invitations => 'users/invitations' }

  devise_scope :user do
    authenticated :user do
      root 'main#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  root to: 'main#index'
end
