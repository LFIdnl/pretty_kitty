class User < ActiveRecord::Base
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  attr_accessor :skip_generate_invitation_token

  def can_resend_invite_to?(user)
    return true if user.invitation_sent_at.nil?
    (Date.today.to_date - user.invitation_sent_at.to_date).to_i > Devise.resend_invitation_during_days
  end
end
