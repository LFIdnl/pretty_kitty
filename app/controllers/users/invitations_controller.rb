class Users::InvitationsController < Devise::InvitationsController
  private

  def invite_resource
    resource_class.invite!(invite_params, current_inviter) do |u|
      unless current_inviter.can_resend_invite_to?(u)
        u.errors.add(:email, :excess_of_resend_invitation_during_days,
                     { email: u.email, days: Devise.resend_invitation_during_days })
      end
    end
  end
end
