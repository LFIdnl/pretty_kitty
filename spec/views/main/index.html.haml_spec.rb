require 'rails_helper'

RSpec.describe "main/index.html.haml", type: :view do
  it "contains kitty picture" do
    render
    expect(rendered).to match("Pretty-Kitty")
  end

  it "contains invite link" do
    render
    expect(rendered).to match(new_user_invitation_path)
  end

  it "contains logout link" do
    render
    expect(rendered).to match(destroy_user_session_path)
  end
end
