require 'rails_helper'

RSpec.describe Users::InvitationsController, type: :controller do

  describe 'POST users/invitation' do
    let(:user) { FactoryGirl.create(:user) }
    let(:valid_attributes) { {user: {email: "testemail@test.com"}} }

    before do
      sign_in user
    end

    it "skips invitation when user can't resend invite" do
      allow(user).to receive(:can_resend_invite_to?).and_return(false)
      post :create, valid_attributes
      invite_resource = controller.send(:invite_resource)
      expect(invite_resource.errors).not_to be_empty
    end

    it "send invitation when user can resend invite" do
      allow(user).to receive(:can_resend_invite_to?).and_return(true)
      post :create, valid_attributes
      invite_resource = controller.send(:invite_resource)
      expect(invite_resource.errors).to be_empty
    end
  end
end
