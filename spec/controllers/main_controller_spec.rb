require 'rails_helper'

RSpec.describe MainController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      sign_in
      get :index
      expect(response).to have_http_status(:success)
    end

    it "redirects to login page if isn't authenticated" do
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end
  end

end
