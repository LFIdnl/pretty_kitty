RSpec.describe User, type: :model do
  describe '#can_resend_invite_to?' do
    let(:invited_user) { FactoryGirl.build(:user, invitation_sent_at: 3.days.ago) }
    let(:inviter_user) { FactoryGirl.build(:user) }

    subject { inviter_user.can_resend_invite_to?(invited_user) }

    context "user already sent invitation to user" do
      it "during specified days" do
        allow(Devise).to receive(:resend_invitation_during_days).and_return(5)
        is_expected.to be_falsey
      end

      it "not during specified days" do
        allow(Devise).to receive(:resend_invitation_during_days).and_return(2)
        is_expected.to be_truthy
      end
    end

    context "user didn't send invitation" do
      it "during specified days" do
        invited_user.invitation_sent_at = nil
        is_expected.to be_truthy
      end
    end
  end
end
